#include <iostream>
using namespace std;

// Fungsi untuk menghitung faktorial
int faktorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * faktorial(n - 1);
}

// Fungsi untuk menghitung permutasi
int permutasi(int n, int r) {
    if (n < r)
        return 0;
    return faktorial(n) / faktorial(n - r);
}

// Fungsi untuk menghitung kombinasi
int kombinasi(int n, int r) {
    if (n < r)
        return 0;
    return faktorial(n) / (faktorial(r) * faktorial(n - r));
}

int main() {
    int n, r;

    cout << "Masukkan nilai n: ";
    cin >> n;

    cout << "Masukkan nilai r: ";
    cin >> r;

    // Menampilkan hasil permutasi
    cout << "Permutasi(" << n << ", " << r << ") = " << permutasi(n, r) << endl;

    // Menampilkan hasil kombinasi
    cout << "Kombinasi(" << n << ", " << r << ") = " << kombinasi(n, r) << endl;

    return 0;
}
